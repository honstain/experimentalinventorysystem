FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.6_10
# FROM alpine:3.9.5
# FROM openjdk:11-jre-alpine

COPY config.yml /data/exp-inventory/config.yml 
COPY /target/exp-inventory-1.0-SNAPSHOT.jar /data/exp-inventory/exp-inventory-1.0-SNAPSHOT.jar

WORKDIR /data/exp-inventory

RUN java -version

CMD ["java", "-Dcom.sun.management.jmxremote", "-Dcom.sun.management.jmxremote.authenticate=false", "-Dcom.sun.management.jmxremote.port=7199", "-Dcom.sun.management.jmxremote.ssl=false", "-jar","exp-inventory-1.0-SNAPSHOT.jar","server","config.yml"]

EXPOSE 8080-8081
EXPOSE 7199
