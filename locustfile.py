from locust import HttpUser, task, between, constant
import json
import random

'''
In order to run this test locally (at least on Ubuntu) you will need the following.

pip3 install locust

'''

class BasicHoldUser(HttpUser):
    api_key = "API-KEY-default"
    wait_time = constant(1) #between(5, 9)

    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.api_key = random.choice(["API-KEY-001", "API-KEY-222", "API-KEY-333"])
        pass

    def on_stop(self):
        """ on_stop is called when the TaskSet is stopping """
        pass

    @task(1)
    def createHold(self):
        payload = {}
        headers = {"content-type": "application/json", "apikey": self.api_key}
        self.client.post("/pond/1/hold/1",
                         data=json.dumps(payload),
                         headers=headers)

