package com.honstain.inventory.api;

import java.util.Objects;

public class Hold implements Comparable<Hold> {
    private Long id;
    private Long pondId;
    private String holdType;
    private String status;
    private String sku;
    private Integer qty;

    public Hold() {
        // Jackson deserialization
    }

    public Hold(Long id, Long pondId, String holdType, String status, String sku, Integer qty) {
        this.id = id;
        this.pondId = pondId;
        this.holdType = holdType;
        this.status = status;
        this.sku = sku;
        this.qty = qty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPondId() {
        return pondId;
    }

    public void setPondId(Long pondId) {
        this.pondId = pondId;
    }

    public String getHoldType() {
        return holdType;
    }

    public void setHoldType(String holdType) {
        this.holdType = holdType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hold hold = (Hold) o;
        return Objects.equals(id, hold.id) &&
                Objects.equals(pondId, hold.pondId) &&
                Objects.equals(holdType, hold.holdType) &&
                Objects.equals(status, hold.status) &&
                Objects.equals(sku, hold.sku) &&
                Objects.equals(qty, hold.qty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pondId, holdType, status, sku, qty);
    }

    @Override
    public int compareTo(Hold hold) {
        return id.compareTo(hold.id);
    }

    @Override
    public String toString() {
        return "Hold{" +
                "id=" + id +
                ", pondId=" + pondId +
                ", holdType='" + holdType + '\'' +
                ", status='" + status + '\'' +
                ", sku='" + sku + '\'' +
                ", qty=" + qty +
                '}';
    }
}
