package com.honstain.inventory.db;

import com.honstain.inventory.api.Hold;
import com.honstain.inventory.api.Pond;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PondAndHoldDao {

    Logger log = LoggerFactory.getLogger(PondAndHoldDao.class);

    private final Jdbi jdbi;
    private final PondTable pondTable;
    private final HoldTable holdTable;

    public PondAndHoldDao(Jdbi jdbi, PondTable pondTable, HoldTable holdTable) {
        this.jdbi = jdbi;
        this.pondTable = pondTable;
        this.holdTable = holdTable;
    }

    public Hold createHoldFromSource(Long sourcePondId, Long sourceHoldId) {
        return jdbi.inTransaction(h -> {
            Pond sourcePond = pondTable.lockAndSelectById(sourcePondId);
            Hold sourceHold = holdTable.lockAndSelectById(sourceHoldId);

            // ***********************************************************
            // Experimental code block, looking at the overhead of pulling all other holds in
            // ***********************************************************
            // TODO - decide how to calculate the total on this thing.
            // TODO - I went with the efficient way to start, but I may want to compare it with calculating it from all holds.
            //long start = System.currentTimeMillis();
            //List<Hold> holdList = holdTable.selectByPondId(sourcePondId);
            //int sellable = 0;
            //int held = 0;
            //for (Hold hold: holdList) {
            //    if (hold.getHoldType().equals("primary")) {
            //        sellable += hold.getQty();
            //    } else if (hold.getHoldType().equals("temp")) {
            //        held += hold.getQty();
            //    }
            //}
            //log.debug("getByPond sellable {} held {} ms {}", sellable, held, System.currentTimeMillis() - start);
            // ***********************************************************

            // Decrement the source hold qty
            Integer sourceHoldQty = sourceHold.getQty();
            if (sourceHoldQty < 1) {
                throw new RuntimeException(String.format("Insufficient qty on sourceHold id: %s", sourceHoldId));
            }
            holdTable.update(sourceHoldId, sourceHold.getPondId(), sourceHold.getStatus(), sourceHoldQty - 1);

            Long newHoldId = holdTable.insert(sourcePondId, "temp", "active", sourcePond.getSku(), 1);
            Hold result = holdTable.lockAndSelectById(newHoldId);
            return result;
        });
    }

}
