package com.honstain.inventory.db;

import com.honstain.inventory.api.Pond;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PondMapper implements RowMapper<Pond> {
    @Override
    public Pond map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Pond(
                rs.getLong("id"),
                rs.getString("sku"),
                rs.getString("location"),
                rs.getInt("qty")
        );
    }
}
