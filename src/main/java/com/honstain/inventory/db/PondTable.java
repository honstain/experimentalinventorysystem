package com.honstain.inventory.db;

import com.honstain.inventory.api.Pond;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.*;

import java.util.List;

public interface PondTable {

    @SqlUpdate(
            "CREATE TABLE IF NOT EXISTS pond (\n" +
            "    id BIGINT PRIMARY KEY AUTO_INCREMENT,\n" +
            "    sku TINYTEXT,\n" +
            "    location TINYTEXT,\n" +
            "    qty int,\n" +
            "    created_at timestamp default current_timestamp\n" +
            ");"
    )
    void createCycleCountTable();

    @SqlUpdate("SET FOREIGN_KEY_CHECKS = 0;")
    void dropFKCheck();
    @SqlUpdate("TRUNCATE table pond;")
    void dropCycleCountTable();
    @SqlUpdate("SET FOREIGN_KEY_CHECKS = 1;")
    void setFKCheck();

    @SqlUpdate(
            "INSERT INTO pond (sku, location, qty) " +
            "VALUES (:sku, :location, :qty)"
    )
    @GetGeneratedKeys
    long insert(
            @Bind("sku") String sku,
            @Bind("location") String location,
            @Bind("qty") Integer qty
    );

    @SqlUpdate(
            "UPDATE pond  " +
            "SET qty = :qty " +
            "WHERE id = :id"
    )
    void updateQty(
            @Bind("id") Long id,
            @Bind("qty") Integer qty
    );

    @SqlQuery("SELECT id, sku, location, qty FROM pond")
    @RegisterRowMapper(PondMapper.class)
    List<Pond> selectAll();

    @SqlQuery("SELECT id, sku, location, qty FROM pond WHERE sku = :sku")
    @RegisterRowMapper(PondMapper.class)
    List<Pond> selectBySku(@Bind("sku") String sku);

    @SqlQuery("SELECT id, sku, location, qty FROM pond WHERE location = :location")
    @RegisterRowMapper(PondMapper.class)
    List<Pond> selectByLocation(@Bind("location") String location);

    @SqlQuery(
            "SELECT id, sku, location, qty " +
            "FROM pond " +
            "WHERE id = :id " +
            "FOR UPDATE")
    @RegisterRowMapper(PondMapper.class)
    Pond lockAndSelectById(@Bind("id") Long id);
}
