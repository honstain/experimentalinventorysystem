package com.honstain.inventory.db;

import com.honstain.inventory.api.Hold;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HoldMapper implements RowMapper<Hold> {
    @Override
    public Hold map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Hold(
                rs.getLong("id"),
                rs.getLong("pondId"),
                rs.getString("holdType"),
                rs.getString("status"),
                rs.getString("sku"),
                rs.getInt("qty")
        );
    }
}
