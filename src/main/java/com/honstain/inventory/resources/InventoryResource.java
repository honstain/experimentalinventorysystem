package com.honstain.inventory.resources;


import com.codahale.metrics.annotation.Timed;
import com.honstain.inventory.api.Hold;
import com.honstain.inventory.api.Pond;
import com.honstain.inventory.db.HoldTable;
import com.honstain.inventory.db.PondAndHoldDao;
import com.honstain.inventory.db.PondTable;
import org.jdbi.v3.core.Jdbi;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class InventoryResource {

    public final Jdbi jdbi;
    public final PondTable pondTable;
    public final HoldTable holdTable;
    public final PondAndHoldDao pondAndHoldDao;

    public InventoryResource(Jdbi jdbi) {
        this.jdbi = jdbi;
        this.holdTable = jdbi.onDemand(HoldTable.class);
        this.pondTable = jdbi.onDemand(PondTable.class);
        this.pondAndHoldDao = new PondAndHoldDao(jdbi, pondTable, holdTable);
    }

    @GET
    @Timed
    @Path("/pond")
    public List<Pond> getAllPonds() {
        List<Pond> result = this.pondTable.selectAll();
        result.sort(Comparator.comparing(Pond::getLocation).thenComparing(Pond::getSku));
        return result;
    }

    @POST
    @Timed
    @Path("/pond")
    public Pond createPond(Pond pond) {
        Long newPondId = this.pondTable.insert(pond.getSku(), pond.getLocation(), pond.getQty());
        Pond newPond = this.pondTable.lockAndSelectById(newPondId);
        return newPond;
    }

    @POST
    @Timed
    @Path("/hold")
    public Hold createHold(Hold hold) {
        Long newHoldId = this.holdTable.insert(hold.getPondId(), hold.getHoldType(), hold.getStatus(), hold.getSku(), hold.getQty());
        Hold newHold = this.holdTable.lockAndSelectById(newHoldId);
        return newHold;
    }

    @POST
    @Timed
    @Path("/pond/{pondId}/hold/{holdId}")
    public Hold createHoldFromSource(@PathParam("pondId") Long pondId, @PathParam("holdId") Long holdId) {
        Hold newHold = this.pondAndHoldDao.createHoldFromSource(pondId, holdId);
        return newHold;
    }

    @GET
    @Timed
    @Path("/slow")
    public void slow() {
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch(InterruptedException ex) { }
    }
}
