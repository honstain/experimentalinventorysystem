package com.honstain.inventory;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import com.honstain.inventory.resources.InventoryResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class ExpInventoryApplication extends Application<ExpInventoryConfiguration> {

    public static void main(final String[] args) throws Exception {
        new ExpInventoryApplication().run(args);
    }

    @Override
    public String getName() {
        return "ExperimentInventory";
    }

    @Override
    public void initialize(final Bootstrap<ExpInventoryConfiguration> bootstrap) {
        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );
    }

    @Override
    public void run(final ExpInventoryConfiguration configuration,
                    final Environment environment) {

        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mariadb");

        environment.jersey().register(new InventoryResource(jdbi));
    }
}
