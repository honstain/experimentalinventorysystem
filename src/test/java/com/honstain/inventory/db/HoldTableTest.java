package com.honstain.inventory.db;

import com.honstain.inventory.api.Hold;
import com.honstain.inventory.api.Pond;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HoldTableTest {

    private static PondTable pondTable;
    private static HoldTable holdTable;

    @BeforeAll
    static void setUpAll() {
        /*
        This approach was from:
            https://github.com/dropwizard/dropwizard/blob/master/dropwizard-example/src/test/java/com/example/helloworld/db/PersonDAOIntegrationTest.java

        I believe it depends on hibernate, that I dont want to use.
         */
        //DAOTestExtension daoTestExtension = DAOTestExtension.newBuilder()
        //        .setDriver("org.mariadb.jdbc.Driver")
        //        .setUrl("jdbc:mariadb://localhost:3306/dropwiz_inventory_test")
        //        .setUsername("inventory")
        //        .setPassword("dev")
        //        .addEntityClass(HoldDao.class)
        //        .addEntityClass(PondDao.class)
        //        .build();

        /*
        This approach was from:
            https://stackoverflow.com/questions/35825383/how-to-test-jdbi-daos-with-h2-in-memory-database

        I am not sure how to make it work.
         */
        //Environment env = new Environment("test-env", Jackson.newObjectMapper(), null, new MetricRegistry(), null, null, null);
        Environment env = new Environment("test-env");
        DataSourceFactory dataSourceFactory = new DataSourceFactory();
        dataSourceFactory.setDriverClass("org.mariadb.jdbc.Driver");
        dataSourceFactory.setUrl("jdbc:mariadb://localhost:3306/dropwiz_inventory_test");
        dataSourceFactory.setUser("inventory");
        dataSourceFactory.setPassword("dev");
        Jdbi jdbi = new JdbiFactory().build(env, dataSourceFactory, "test-mysql-jdbi");

        /*
        The original approach I started with.
         */
        //Jdbi jdbi = Jdbi.create("jdbc:mariadb://localhost:3306/dropwiz_inventory_test", "inventory", "dev");
        //Jdbi jdbi = new JdbiFactory().build(
        //        DROPWIZARD.getEnvironment(),
        //        DROPWIZARD.getConfiguration().getDataSourceFactory(),
        //        "mariadb-test"
        //);
        pondTable = jdbi.onDemand(PondTable.class);
        pondTable.createCycleCountTable();

        holdTable = jdbi.onDemand(HoldTable.class);
        holdTable.createCycleCountTable();
    }

    @BeforeEach
    void setup() {
        holdTable.dropFKCheck();
        holdTable.dropCycleCountTable();
        pondTable.dropCycleCountTable();
        holdTable.setFKCheck();
    }

    @Test
    void testSelectAllEmpty() {
        assertEquals(List.of(), holdTable.selectAll());
    }

    @Test
    void testInsertAndSelectAll() {
        final Pond pond = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond.setId(
            pondTable.insert(
                    pond.getSku(),
                    pond.getLocation(),
                    pond.getQty()
            ));

        final Hold hold = new Hold(1L, pond.getId(), "temp", "active", "SKU-01", 1);
        hold.setId(
                holdTable.insert(
                        hold.getPondId(),
                        hold.getHoldType(),
                        hold.getStatus(),
                        hold.getSku(),
                        hold.getQty()
                ));
        assertEquals(List.of(hold), holdTable.selectAll());
    }

    @Test
    void testInsertAndSelectByPondId() {
        final Pond pond = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond.setId(
                pondTable.insert(
                        pond.getSku(),
                        pond.getLocation(),
                        pond.getQty()
                ));

        final Hold hold1 = new Hold(1L, pond.getId(), "temp", "active", "SKU-01", 1);
        hold1.setId(
                holdTable.insert(
                        hold1.getPondId(),
                        hold1.getHoldType(),
                        hold1.getStatus(),
                        hold1.getSku(),
                        hold1.getQty()
                ));
        final Hold hold2 = new Hold(1L, pond.getId(), "temp", "active", "SKU-02", 1);
        hold2.setId(
                holdTable.insert(
                        hold2.getPondId(),
                        hold2.getHoldType(),
                        hold2.getStatus(),
                        hold2.getSku(),
                        hold2.getQty()
                ));
        assertEquals(List.of(hold1, hold2), holdTable.selectByPondId(pond.getId()));
    }

    @Test
    void testInsertAndSelectForUpdate() {
        final Pond pond = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond.setId(
                pondTable.insert(
                        pond.getSku(),
                        pond.getLocation(),
                        pond.getQty()
                ));

        final Hold hold = new Hold(1L, pond.getId(), "temp", "active", "SKU-01", 1);
        hold.setId(
                holdTable.insert(
                        hold.getPondId(),
                        hold.getHoldType(),
                        hold.getStatus(),
                        hold.getSku(),
                        hold.getQty()
                ));
        assertEquals(hold, holdTable.lockAndSelectById(hold.getId()));
    }

    @Test
    void testUpdate() {
        final Pond pond1 = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond1.setId(
                pondTable.insert(
                        pond1.getSku(),
                        pond1.getLocation(),
                        pond1.getQty()
                ));
        final Pond pond2 = new Pond(1L, "SKU-01", "LOC-02", 2);
        pond2.setId(
                pondTable.insert(
                        pond2.getSku(),
                        pond2.getLocation(),
                        pond2.getQty()
                ));

        final Hold hold = new Hold(1L, pond1.getId(), "temp", "active", "SKU-01", 1);
        hold.setId(
                holdTable.insert(
                        hold.getPondId(),
                        hold.getHoldType(),
                        hold.getStatus(),
                        hold.getSku(),
                        hold.getQty()
                ));
        assertEquals(List.of(hold), holdTable.selectAll());

        holdTable.update(hold.getId(), pond2.getId(), "deleted", 2);
        hold.setPondId(pond2.getId());
        hold.setStatus("deleted");
        hold.setQty(2);
        assertEquals(List.of(hold), holdTable.selectAll());
    }
}
