package com.honstain.inventory.db;

import com.honstain.inventory.api.Pond;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PondTableTest {

    private static PondTable pondTable;

    @BeforeAll
    static void setUpAll() {
        Environment env = new Environment("test-env");
        DataSourceFactory dataSourceFactory = new DataSourceFactory();
        dataSourceFactory.setDriverClass("org.mariadb.jdbc.Driver");
        dataSourceFactory.setUrl("jdbc:mariadb://localhost:3306/dropwiz_inventory_test");
        dataSourceFactory.setUser("inventory");
        dataSourceFactory.setPassword("dev");
        Jdbi jdbi = new JdbiFactory().build(env, dataSourceFactory, "test-mysql-jdbi");

        pondTable = jdbi.onDemand(PondTable.class);
        pondTable.createCycleCountTable();
    }

    @BeforeEach
    void setup() {
        pondTable.dropFKCheck();
        pondTable.dropCycleCountTable();
        pondTable.setFKCheck();
    }

    @Test
    void testSelectAllEmpty() {
        assertEquals(List.of(), pondTable.selectAll());
    }

    @Test
    void testInsertAndSelectAll() {
        String SKU = "SKU-01";
        final Pond pond1 = new Pond(1L, SKU, "LOC-01", 4);
        pond1.setId(
                pondTable.insert(
                        pond1.getSku(),
                        pond1.getLocation(),
                        pond1.getQty()
                ));
        final Pond pond2 = new Pond(1L, SKU, "LOC-02", 2);
        pond2.setId(
                pondTable.insert(
                        pond2.getSku(),
                        pond2.getLocation(),
                        pond2.getQty()
                ));

        assertEquals(List.of(pond1, pond2), pondTable.selectAll());
    }

    @Test
    void testInsertAndSelectBySku() {
        String SKU = "SKU-01";
        final Pond pond1 = new Pond(1L, "SKU-02", "LOC-01", 4);
        pond1.setId(
                pondTable.insert(
                        pond1.getSku(),
                        pond1.getLocation(),
                        pond1.getQty()
                ));
        final Pond pond2 = new Pond(1L, SKU, "LOC-02", 2);
        pond2.setId(
                pondTable.insert(
                        pond2.getSku(),
                        pond2.getLocation(),
                        pond2.getQty()
                ));

        assertEquals(List.of(pond2), pondTable.selectBySku(SKU));
    }

    @Test
    void testInsertAndSelectByLocation() {
        String LOCATION = "LOC-01";
        final Pond pond1 = new Pond(1L, "SKU-01", LOCATION, 4);
        pond1.setId(
                pondTable.insert(
                        pond1.getSku(),
                        pond1.getLocation(),
                        pond1.getQty()
                ));
        final Pond pond2 = new Pond(1L, "SKU-02", LOCATION, 2);
        pond2.setId(
                pondTable.insert(
                        pond2.getSku(),
                        pond2.getLocation(),
                        pond2.getQty()
                ));

        assertEquals(List.of(pond1, pond2), pondTable.selectByLocation(LOCATION));
    }

    @Test
    void testInsertAndSelectForUpdate() {
        final Pond pond = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond.setId(
                pondTable.insert(
                        pond.getSku(),
                        pond.getLocation(),
                        pond.getQty()
                ));

        assertEquals(pond, pondTable.lockAndSelectById(pond.getId()));
    }

    @Test
    void testUpdateQty() {
        final Pond pond = new Pond(1L, "SKU-01", "LOC-01", 4);
        pond.setId(
                pondTable.insert(
                        pond.getSku(),
                        pond.getLocation(),
                        pond.getQty()
                ));

        assertEquals(List.of(pond), pondTable.selectAll());

        pondTable.updateQty(pond.getId(), 2);
        pond.setQty(2);
        assertEquals(List.of(pond), pondTable.selectAll());
    }
}
