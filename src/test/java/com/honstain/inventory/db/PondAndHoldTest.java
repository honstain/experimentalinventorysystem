package com.honstain.inventory.db;

import com.honstain.inventory.api.Hold;
import com.honstain.inventory.api.Pond;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PondAndHoldTest {

    private static PondTable pondTable;
    private static HoldTable holdTable;
    private static PondAndHoldDao pondAndHoldDao;

    @BeforeAll
    static void setUpAll() {
        Environment env = new Environment("test-env");
        DataSourceFactory dataSourceFactory = new DataSourceFactory();
        dataSourceFactory.setDriverClass("org.mariadb.jdbc.Driver");
        dataSourceFactory.setUrl("jdbc:mariadb://localhost:3306/dropwiz_inventory_test");
        dataSourceFactory.setUser("inventory");
        dataSourceFactory.setPassword("dev");
        Jdbi jdbi = new JdbiFactory().build(env, dataSourceFactory, "test-mysql-jdbi");

        pondTable = jdbi.onDemand(PondTable.class);
        pondTable.createCycleCountTable();

        holdTable = jdbi.onDemand(HoldTable.class);
        holdTable.createCycleCountTable();

        pondAndHoldDao = new PondAndHoldDao(jdbi, pondTable, holdTable);
    }

    @BeforeEach
    void setup() {
        holdTable.dropFKCheck();
        holdTable.dropCycleCountTable();
        pondTable.dropCycleCountTable();
        holdTable.setFKCheck();
    }

    @Test
    void testCreateHoldFromSource() {
        String SKU = "SKU-01";
        final Pond pond1 = new Pond(1L, SKU, "LOC-01", 4);
        pond1.setId(
                pondTable.insert(
                        pond1.getSku(),
                        pond1.getLocation(),
                        pond1.getQty()
                ));
        final Pond pond2 = new Pond(1L, SKU, "LOC-02", 2);
        pond2.setId(
                pondTable.insert(
                        pond2.getSku(),
                        pond2.getLocation(),
                        pond2.getQty()
                ));

        final Hold primaryHold = new Hold(1L, pond1.getId(), "primary", "active", SKU, 1);
        primaryHold.setId(
                holdTable.insert(
                        primaryHold.getPondId(),
                        primaryHold.getHoldType(),
                        primaryHold.getStatus(),
                        primaryHold.getSku(),
                        primaryHold.getQty()
                ));

        final Hold newHold = pondAndHoldDao.createHoldFromSource(pond1.getId(), primaryHold.getId());
        primaryHold.setQty(0);
        assertEquals(List.of(primaryHold, newHold), holdTable.selectAll());
    }
}
